.. index::
   pair: Iran : le régime tremble de toute part ; Jean-Pierre Perrin (Mediapart, 2025-01-16)

.. _perrin_2025_01_18:

============================================================================================
2025-01-16 **Iran : le régime tremble de toute part** par Jean-Pierre Perrin (Mediapart)
============================================================================================

- https://www.mediapart.fr/journal/international/160125/iran-le-regime-tremble-de-toute-part

Auteur: Jean-Pierre Perrin
======================================

- https://www.mediapart.fr/biographie/jean-pierre-perrin

Longtemps grand reporter à Libération, travaillant sur le Proche et Moyen-Orient. 

A présent, journaliste indépendant et écrivain. 

Auteur de romans policiers, dont Chiens et Louves (Gallimard - Série noire) 
et Une guerre sans fin (Rivages noir), de récits de guerre, dont:
 
- Afghanistan: jours de poussière (La Table Ronde - grand prix des lectrices de Elle en 2003) 
- Les Rolling Stones sont à Bagdad (Flammarion - 2003) 
- La mort est ma servante, lettre à un ami assassiné - Syrie 2005 - 2013 (Fayard - 2013) 
- Le djihad contre le rêve d'Alexandre (Le Seuil - prix Joseph Kessel - 2017) 


Article au format PDF
===========================

:download:`Télécharger l'article "Iran : le régime tremble de toute part" au format PDF <pdfs/iran_tremble_de_toute_part.pdf>`


Préambule
==============

Pénuries de gaz et d’électricité, usines à l’arrêt, fermetures des écoles 
et des administrations, chute vertigineuse de la monnaie : **la République
islamique s’enfonce dans une crise sans précédent**. 

Après sa débâcle en Syrie, Téhéran s’inquiète du retour de Donald Trump à 
la Maison-Blanche.

Jean-Pierre Perrin

Iran : le régime tremble de toute part
========================================

On dirait un pays pétrifié. 

**À cause des températures hivernales** que les autorités ne semblent pas avoir 
anticipées, et qui ont été particulièrement basses sur l’ensemble du 
territoire iranien, notamment en décembre, où elles ont par endroits 
atteint -20 °C. 

**À cause aussi de la pénurie d’énergie sans précédent, gaz et électricité à 
la fois**, conséquence notamment des aléas climatiques, qui a affecté la majorité 
des provinces, provoquant la fermeture des écoles, des universités et des 
administrations pendant plusieurs jours, et entraînant des manifestations 
dans plusieurs localités.

**À cause encore de la pollution**, de plus en plus insupportable dans les grandes
villes, de la paralysie de l’économie, d’une inflation à près de 35 %,
de la chute sans fin de la monnaie (aujourd’hui il faut 800 000 rials pour un
dollar), **conséquence de la perte de la Syrie, de la défaite du Hezbollah et,
surtout, de l’arrivée prochaine au pouvoir de Donald Trump**.

"Quand les écoles et les administrations ne ferment pas à cause du
manque de chauffage, elles y sont contraintes à cause de la pollution,
déplore Afsaneh, une habitante de Téhéran dont l’entreprise familiale de
confection ne travaille plus qu’à 30 % de sa capacité, en raison du manque
d’électricité. C’était encore le cas samedi et dimanche derniers. Il y
a toujours une raison pour que le pays soit à l’arrêt. 

**Quand les gens se rencontrent dans la rue ou ailleurs, la première chose 
qu’ils se disent, c’est : “Tu te rends compte de ce qui nous arrive !”"**

On sent le nizem [régime – ndlr] paralysé à l’idée du retour au pouvoir de Donald Trump, avec pour conséquence **une possible attaque israélo-américaine**
-------------------------------------------------------------------------------------------------------------------------------------------------------------

"C’est jusqu’au régime qui est complètement pétrifié, reprend Afsaneh. 
Alors que la situation est totalement dramatique à tous les points de
vue, il ne prend aucune décision. Aucun responsable ne s’exprime sur aucun
des problèmes de l’Iran. 
**On sent le nizem [régime – ndlr] paralysé à l’idée du retour au pouvoir de 
Donald Trump, avec pour conséquence une possible attaque israélo-américaine."**

**"Tout le monde dit que cela ne peut plus durer, insiste Maryam, une autre
Iranienne. On se dit qu’il va forcément se passer quelque chose. Mais personne
ne sait quoi."**

**Industries à l’arrêt** 
==========================

Cette pénurie d’énergie, avec un déficit quotidien d’au moins 260 millions 
de mètres cubes de gaz par jour, selon des chiffres officiels, porte un coup 
terrible à une économie déjà exsangue. 

Si les politiques font silence, les responsables de certains secteurs économiques,
en revanche, s’alarment. Dans une interview accordée fin décembre à la
télévision d’État, un dirigeant de l’immense complexe industriel d’Abbas
Abad, au sud-est de Téhéran, faisait ainsi savoir que les **industries qui y
sont localisées étaient confrontées à des coupures de courant pouvant aller
jusqu’à quatorze heures par jour**.

Dans l’industrie sidérurgique, les pertes annuelles dues à la pénurie sont
estimées à environ 4 milliards de dollars par le directeur de la chambre de
commerce d’Ispahan, Amir Kashani, cité par l’agence économique iranienne
Bourse Press. Et selon le représentant des employeurs au Conseil suprême du
travail, Ali-Asghar Ahaniha, "50 % des parcs industriels iraniens ont été
contraints de cesser leurs activités en raison de coupures de courant".

Pas moins de 80 centrales électriques ont aussi fermé, a-t-il ajouté dans une
interview accordée, le 11 janvier, à Tasnim, une agence de presse contrôlée
par les Pasdaran (Gardiens de la révolution). Les élevages industriels de
poulets et les laiteries sont également frappés de plein fouet.

Ironie de la situation, cette année – qui se terminera le 20 mars selon le
calendrier iranien – avait été surnommée par le pouvoir "l’année du bond en 
avant de la production".

**L’appel au secours d’Olivier Grondeau, détenu en Iran**  |OlivierGrondeau|
==================================================================================


Appel à l'aide d'Olivier Grondeau depuis la prison d'Evin (liens NDLR)
------------------------------------------------------------------------

- :ref:`iran_luttes:olivier_grondeau`
- :ref:`iran_luttes:cecile_kohler`
- :ref:`iran_luttes:jacques_paris`
- https://www.mediapart.fr/journal/international/051224/des-temoins-racontent-l-enfer-des-prisons-de-la-dictature-islamique-d-iran

- https://libertepourolivier.fr/appel-a-l-aide
- https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3

Cliquer sur https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3 
pour écouter l'appel d'Olivier Grondeau depuis la prison d'Evin.

------------

C’est un appel à l’aide des plus dramatique qu’a lancé le 9 janvier 2025 depuis
la prison d’Evin, près de Téhéran, l’otage français Olivier Grondeau,
dont le nom était jusqu’alors tenu secret, pour alerter sur la détresse de
ses deux codétenu·es, ainsi que la sienne, et appeler à leur libération. 
Et ce en autorisant sa famille à diffuser sur les antennes de Radio France des
extraits de conversations téléphoniques échangées avec elle. 
Le Quai d’Orsay a donné son aval à cette diffusion.

Arrêté en octobre 2022 et condamné à cinq ans de prison, le touriste français,
âgé de 34 ans, a décidé de rompre le silence après le rejet de sa demande de
libération conditionnelle : "Je m’appelle Olivier, je suis retenu en otage
depuis deux ans et trois mois par le gouvernement iranien. Dans ma situation,
prendre la parole, c’est prendre un risque. Mais comme il y a risque, il y a
espoir. Mais il m’en reste très peu. Je suis vraiment très fatigué."

"Vous qui avez le pouvoir d’influer sur cette affaire, entendez cette
vérité : les forces de Cécile, les forces de Jacques, les forces d’Olivier
s’épuisent", a-t-il aussi déclaré, en référence à ses deux compatriotes
`Cécile Kohler et Jacques Paris <https://www.mediapart.fr/journal/international/051224/des-temoins-racontent-l-enfer-des-prisons-de-la-dictature-islamique-d-iran>`_, 
également détenu·es en Iran depuis 2022.

------------

Voir aussi :ref:`iran_luttes_2024:prisons_iran_2024_12_09`  (NDLR)

------------


Nahid Taghavi
------------------

Sa sortie de l’anonymat a précédé de quelques heures l’annonce par sa
famille de la libération de la Germano-Iranienne Nahid Taghavi, après quatre
ans de détention. 

Cecilia Sala
--------------

- https://www.humanite.fr/monde/iran/cecilia-sala-la-journaliste-italienne-detenue-en-iran-a-ete-liberee

Et elle suit de quelques jours la sortie de prison et le retour
chez elle d’une journaliste italienne, `Cecilia Sala <https://www.humanite.fr/monde/iran/cecilia-sala-la-journaliste-italienne-detenue-en-iran-a-ete-liberee>`_, 
arrêtée alors qu’elle se trouvait légalement en Iran avec un visa de presse. 

Sa libération fait suite à celle de Mohammad Abedini, un Iranien arrêté à 
Milan sur ordre des États-Unis, qui l’accusent d'avoir contourné les sanctions 
américaines contre l’Iran.


Ce qui met en rage une large partie de la population, c’est que l’Iran possède les deuxièmes plus grandes réserves de gaz au monde et les troisièmes plus grandes réserves de pétrole
============================================================================================================================================================================================

Ce qui met en rage une large partie de la population, c’est que l’Iran
possède les deuxièmes plus grandes réserves de gaz au monde et les troisièmes
plus grandes réserves de pétrole. 

Mais elle ne retient pas comme cause de son malheur les sanctions économiques 
internationales, en particulier américaines, pourtant largement responsables 
du manque d’investissements dans le secteur de l’énergie, à commencer par les 
raffineries, et de l’absence de rénovation d’infrastructures à bout de souffle 
– leur obsolescence entraîne la perte de près de 25 % du gaz produit dans le pays.


Ce qu’elle dénonce, on le voit à la lecture de certains titres de la presse iranienne, **c’est l’aide phénoménale que le régime a accordée au régime de Bachar al-Assad pour assurer sa survie ces quinze dernières années**
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Ce qu’elle dénonce, on le voit à la lecture de certains titres de la presse
iranienne, **c’est l’aide phénoménale que le régime a accordée au régime
de Bachar al-Assad pour assurer sa survie ces quinze dernières années**. 

**À fonds perdus.**

À ce sujet, les journaux ont avancé des chiffres : quelque 30 milliards de 
dollars de prêts et d’aide militaire. Voire 50 milliards, selon des économistes 
occidentaux.

**Les services de sécurité en état d’alerte**
================================================

Il faut y ajouter les milliards investis dans le projet d’hégémonie régionale, 
dont le financement de ce que le régime appelle "l’axe de la résistance", 
qui va de Téhéran à Beyrouth, en passant par Bagdad, et inclut Sanaa, la 
capitale du Yémen, contrôlée par les rebelles houthis, "axe" qui paraît 
complètement désarticulé depuis le changement de régime en Syrie. 

Ces sommes, à l’échelle de l’Iran, sont considérables.  

Curieusement, c’est le ministre de la justice islamique, le religieux 
ultraconservateur Gholamhossein Mohseni-Ejei, qui est intervenu pour déplorer 
l’actuelle situation, notamment l’incapacité pour certaines entreprises 
frappées par les pénuries d’énergie de payer leurs salarié·es – on observe 
déjà une multiplication de grèves. 

Aussi a-t-il d’ores et déjà alerté les services de sécurité pour leur demander
de se tenir en état d’alerte, au cas où la situation se détériorerait
davantage, pour réprimer d’éventuels soulèvements de la faim, comme `cela
s’était produit en 2019-2020 <https://www.mediapart.fr/journal/international/231119/iran-les-manifestants-mates-la-repression-feroce-commence>`_.  

Mais le commandement du corps des Gardiens de la révolution islamique n’a 
pas ce seul souci. 
Il montre à présent sa crainte qu’une fois Donald Trump installé à la Maison-Blanche, l’Iran fasse
l’objet d’une attaque israélo-américaine en règle. Les récentes frappes
de Tel-Aviv contre les défenses antiaériennes de l’Iran et ses capacités
de production de missiles balistiques ont beaucoup inquiété à Téhéran.

La stratégie révolutionnaire cède le pas à une stratégie de défense
nationale, organisée depuis le sol iranien.

D’où l’annonce, le 25 décembre 2024, par le général Gholamali Rachid,
qui est à la tête du Khatam al-Anbiya, l’organisme qui supervise les
opérations militaires au sein de la République islamique, de manœuvres 
"massives" et planifiées à l’échelle nationale, à la fois "offensives et
défensives", sur terre, en mer et dans les airs, menées aussi bien par les
Pasdaran que par l’artesh, l’armée régulière.  

Baptisées "Eqtedar", "puissance" en persan, ces opérations dureront plusieurs mois (jusqu’à la mi-mars)
------------------------------------------------------------------------------------------------------------

Baptisées "Eqtedar", "puissance" en persan, ces opérations dureront plusieurs 
mois (jusqu’à la mi-mars). Elles sont une façon pour l’Iran de montrer 
"la puissance de son équipement militaire face aux menaces israéliennes". 

Mais en voulant faire de ces manœuvres un "moyen de dissuasion" et en leur 
donnant une large publicité, c’est plutôt de sa faiblesse que le régime témoigne.
Ces manœuvres sont les premières depuis le revirement stratégique décidé
par la République islamique, qui a renoncé au concept de "défense avancée" 
qu’elle privilégiait jusqu’alors, c’est-à-dire à la protection du
territoire depuis l’extérieur, via ses mandataires ou proxies. 


Désormais, la stratégie révolutionnaire cède le pas à une stratégie de défense nationale, donc organisée depuis le sol iranien
------------------------------------------------------------------------------------------------------------------------------------

Désormais, la stratégie révolutionnaire cède le pas à une stratégie de défense 
nationale, donc organisée depuis le sol iranien, ce que le régime avait déjà 
commencé à mettre en place avant la déroute syrienne, comme l’a montré le tir,
pour la première fois depuis le territoire iranien (et non par le Hezbollah),
dans la nuit du 13 au 14 avril 2014, de quelque 350 missiles et drones sur Israël.

Dans un système des plus opaque, percent aussi les premières dissensions
jamais observées parmi les partisans les plus fidèles du Guide suprême et
de la ligne la plus dure. Alors que toute critique contre Ali Khamenei est
rigoureusement interdite et passible de prison, **Bisimchimedia**, un petit média
proche des services de renseignement des Pasdaran, s’est permis, pour la
première fois semble-t-il, **de l’accuser de passivité dans sa réponse aux
attaques israéliennes**. 

En réaction, **Tasnim News**, l’agence plus officielle de l’armée idéologique 
du régime, a lancé, publiquement, **un avertissement contre tout manquement 
de loyauté à son égard**.

**Suspension de la loi "hidjab et chasteté"** 
===============================================

**Tout montre que le pouvoir est sur le qui-vive**. 

On observe ainsi un mutisme sans précédent des responsables iraniens
vis-à-vis des États-Unis et même les déclarations du Guide suprême à son
égard ont perdu de leur superbe. 

Ainsi regrettait-il récemment que Washington manifeste toujours une "rancune de chameau" 
(le chameau est supposé être très rancunier) envers la République islamique, 
laissant entendre que pouvoir iranien n’avait, en revanche, aucun grief 
sérieux à son égard et que lui-même ne s’opposerait pas à des négociations 
avec Donald Trump. 

La reprise des discussions sur le nucléaire à Vienne (Autriche), le 12 janvier 2025, 
témoigne aussi des inquiétudes de Téhéran.  

Cette fragilité du pouvoir s’explique également par la **crainte d’un réveil de la rue, comme à l’automne 2022 après la mort de Mahsa Amini**
-----------------------------------------------------------------------------------------------------------------------------------------------

Cette fragilité du pouvoir s’explique également par la **crainte d’un réveil 
de la rue, comme à l’automne 2022 après la mort de Mahsa Amini**. 

Certes, la répression demeure absolument féroce, comme le montre la condamnation 
à mort, il y a quelques jours, de :ref:`Pakhshan Azizi <pakhshan_azizi_2025_01_28>`, une jeune humanitaire kurde, 
pour des activités relevant des droits humains les plus élémentaires, comme 
**porter assistance à des femmes et à des enfants déplacé·es**.  

Cependant, à la surprise générale, le Conseil des gardiens de la Constitution 
islamique (l’équivalent d’un Conseil constitutionnel) a suspendu, le 15 décembre 2024, 
la nouvelle "loi sur le hidjab et la chasteté" qui **avait été votée massivement 
par le Majlis (le Parlement)** l’année précédente et s’avérait particulièrement 
répressive pour les bi-hidjab, les femmes se refusant à porter le voile obligatoire.

"Toute personne" qui commet le délit de "ne pas porter de voile ou de
porter des vêtements inappropriés en coopération avec des gouvernements,
des médias, des groupes ou des organisations étrangères ou hostiles" à la
République islamique, "ou de manière organisée, sera condamnée à **une peine
d’emprisonnement du quatrième degré", soit de cinq à dix ans**, prévoyait
le texte, voté, **dans un souci de provocation des Iraniennes, pour le premier
anniversaire du décès de la jeune Kurde**.  

Le fait que le Guide suprême ait consenti à la suspension d’une loi que lui-même 
appelait de ses vœux et que cette suspension ait été validée par le Conseil 
national de sécurité, la plus haute instance chargée de la sécurité intérieure 
et extérieure, montre un régime sur la défensive. 


**L’obligation de porter le voile étant la clé de voûte du système**, celui-ci n’y a cependant nullement renoncé
----------------------------------------------------------------------------------------------------------------------

**L’obligation de porter le voile étant la clé de voûte du système**, celui-ci 
n’y a cependant nullement renoncé.

"En attendant, **les Iraniennes sans voile sont à présent majoritaires dans
les rues de Téhéran**, quel que soit le quartier, souligne Afsaneh. 

Mais moi je le porte. J’ai déjà suffisamment de problèmes avec eux."

Jean-Pierre Perrin.

