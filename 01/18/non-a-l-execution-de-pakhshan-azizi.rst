.. index::
   ! Non à l'exécution de Pakhshan Azizi (2025-01-18)

.. _pakhshan_azizi_2025_01_28:

===================================================================================================================================================================
2025-01-18 ✊ ⚖️ 📣  **Non à l'exécution de Pakhshan Azizi**  |PakhshanAzizi| rassemblement place Félix Poulat à Grenoble le samedi 18 janvier 2025 à 14h30
===================================================================================================================================================================

.. tags:: StopExecutionsInIran, PakhshanAzizi, VarisheMoradi, BehrouzEhsani, MehdiHassani, FreeCecileKohler, FreeJacquesParis , FreeOlivierGrondeau, FreeThemAll 

🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 ) 
🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté #FemmeVieLiberte (Français, 🇫🇷)
🇮🇹 Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪) 

#IranRevolution #IranProtests2025 #ZanZendegiÂzâdi #JinJiyanAzadî #WomanLifeFreedom #FemmeVieLiberte 
#PakhshanAzizi #VarisheMoradi #BehrouzEhsani #MehdiHassani #NoToExecutionTuesdays #StopExecutionsInIran
#FreeCecileKohler #FreeJacquesParis #FreeOlivierGrondeau #FreeThemAll 


Annonce sur le fediverse (mastodon et pixelfed)
=====================================================

- https://kolektiva.social/@iranluttes/113842081581209166
- https://pixelfed.fr/i/web/post/785752631514434324

Lieu du rassemblement: la place Félix Poulat de Grenoble le samedi 18 janvier 2025 à 14h30
================================================================================================

.. raw:: html
   
   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.7244858145713815%2C45.1890863465933%2C5.72802633047104%2C45.19069122153978&amp.. figure:: images/layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.189889/5.726256">Afficher une carte plus grande</a></small>


**Non à l'exécution de Pakhshan Azizi** |PakhshanAzizi| rassemblement place Félix Poulat le samedi 18 janvier 2025 à 14h30
===========================================================================================================================


- https://en.wikipedia.org/wiki/Pakhshan_Azizi
- https://wncri.org/fr/2024/07/24/pakhshan-azizi/
- https://wncri.org/fr/2025/01/15/des-militantes-sanandadj-pakhshan-azizi/
- https://csdhi.org/actualites/repression/48572-iran-la-rapporteuse-speciale-des-nations-unies-sur-les-droits-humains-condamne-la-decision-dexecuter-pakhshan-azizi/
- https://csdhi.org/actualites/repression/48552-iran-les-experts-de-lonu-salarment-de-la-confirmation-par-la-cour-supreme-de-la-condamnation-a-mort-dune-militante-kurde/
- https://iranhumanrights.org/2025/01/un-iran-must-immediately-revoke-death-sentence-against-kurdish-woman-activist/

**Non à l'exécution de Pakhshan Azizi, Le temps presse**
----------------------------------------------------------

.. figure:: images/pakhshan_azizi_death_confirmed.webp

.. figure:: images/le_temps_presse.webp
   :width: 500


Le régime iranien continue à appliquer la peine de mort.

En moyenne il ya deux à trois exécutions par jour ( 940 en 2024 ). 
Le régime exécute aussi ses opposants.

Aujourd’hui 54 prisonniers politiques sont dans les couloirs de la mort 
dont **Pakhshan Azizi jeune femme kurde travailleuse humanitaire, Behrouze Ehsani 
et Mehdi Hassani**. 

.. figure:: images/pakhshan_azizi_and_others.webp
   :width: 500

La cour suprême a en eﬀet confirmé leur condamnation à la peine capitale. 
En condamnant :ref:`madame Pakhshan Azizi <pakhshan_azizi_2025_01_18>` |PakhshanAzizi| et :ref:`madame Varisheh Moradi <varishe_moradi_2025_01_18>` |VarisheMoradi| 
**la République  Islamique veut faire peur aux femmes et pense pouvoir les 
réduire au silence**.

Soyons la voix des condamnés et exigeons l’annulation des condamnations et 
l’arrêt des exécutions.

À l’appel et avec le soutien de:

- LD Iran, 
- LDH Grenoble, 
- Iran Solidarité,
- Mouvement de la paix , 
- Amnesty, 
- Cisem, 
- Cercle Laïque, 
- AIAK (Association Iseroise des Amis des Kurdes), 
- Csra, 
- Acat...

:download:`Télécharger le tract au format PDF : "Empêchons l'exécution de Pakhshan Azizi, le temps presse" <pdfs/rassemblement_pakhshan_azizi_2025_01_18.pdf>`


.. _pakhshan_azizi_2025_01_18:

Death Sentence Confirmed for Kurdish Political Prisoner Pakhshan Azizi |PakhshanAzizi|
============================================================================================

- https://iran-hrm.com/2025/01/08/death-sentence-confirmed-for-kurdish-political-prisoner-pakhshan-azizi/

.. figure:: images/pakhshan_azizi_death_confirmed.webp

The Iranian Supreme Court has upheld the death sentence for Kurdish political
prisoner Pakhshan Azizi, despite significant procedural flaws. 
Her lawyer, Amir Raisiyan, revealed that the court disregarded evidence pointing 
to her peaceful activities in refugee camps in northern Syria, which were 
mischaracterized as political crimes.

**Azizi, a social work graduate and activist from Mahabad**, was arrested in Tehran
in August 2023 and tortured during interrogation to force a confession. 
She was accused of “rebellion through membership in opposition groups” and 
sentenced in June 2024 by Judge Iman Afshari of Branch 26 of the Tehran Revolutionary Court.

**Azizi has a history of activism, including a 2009 arrest during student protests
against political executions in Kurdistan**. 
Released on bail in 2010, she was rearrested in 2023 on similar charges.

**Iran HRM calls for urgent intervention in this case**. 

The use of torture, forced confessions, and flawed judicial proceedings 
underscores the Iranian regime’s systematic violations of human rights.


.. _varishe_moradi_2025_01_18:

2024-11-10 **La prisonnière politique Varishe Moradi** |VarisheMoradi| **condamnée à mort par le tribunal révolutionnaire de Téhéran**
=========================================================================================================================================

- https://csdhi.org/actualites/prisoniers-politiques/47957-la-prisonniere-politique-varishe-moradi-condamnee-a-mort-par-le-tribunal-revolutionnaire-de-teheran/

.. figure:: images/varishe_moradi_death_confirmed.webp


Political Prisoners in Iran Urge Action to Halt Executions of Fellow Activists
===================================================================================

- https://iran-hrm.com/2025/01/11/political-prisoners-in-iran-urge-action-to-halt-executions-of-fellow-activists/

.. figure:: images/pakhshan_azizi_and_others.webp


In a forceful statement issued from Qezel-Hesar Prison, a group of political
prisoners has condemned the Iranian governments increasing reliance on executions
as a tool of repression. 
The statement specifically denounces the recent Supreme Court decision upholding 
death sentences for three political prisoners, Pakhshan Azizi, Behrouz Ehsani, 
and Mehdi Hassani, and calls for urgent international action to save their lives.

The political prisoners, who include Saeed Masouri, Sepehr Emam Jomeh, Mohammad
Shafei, Meysam Dehbanzadeh, and Loghman Aminpour, highlight the regime’s use of
capital punishment to suppress dissent and intimidate Iranian society. 

Despite these efforts, they emphasize the resilience and defiance of those resisting
oppression, including the three condemned individuals, who were active in
the “No to Execution Tuesdays” campaign—a grassroots movement to halt
state-sanctioned killings.

The Death Sentences and Broader Context
-----------------------------------------------

- https://iran-hrm.com/2025/01/08/iranian-judiciary-confirms-death-sentences-for-political-prisoners/
- https://iran-hrm.com/2025/01/08/death-sentence-confirmed-for-kurdish-political-prisoner-pakhshan-azizi/

The Supreme Court recently upheld the death sentences of `Ehsani and Hassani <https://iran-hrm.com/2025/01/08/iranian-judiciary-confirms-death-sentences-for-political-prisoners/>`_
on charges of “rebellion,” “waging war against God” (moharebeh), and
affiliation with the People’s Mojahedin Organization of Iran (PMOI/MEK),
a group banned by the Iranian government. 
**These charges are frequently used by Iranian authorities to target political 
activists**. 
`Pakhshan Azizi <https://iran-hrm.com/2025/01/08/death-sentence-confirmed-for-kurdish-political-prisoner-pakhshan-azizi/>`_, faces similar accusations.

This development comes amid reports of more than 50 other political prisoners
currently on death row in Iran, as the regime intensifies its crackdown on dissent.

The statement attributes these actions to Iran’s Supreme Leader, Ayatollah Ali
Khamenei, accusing him of orchestrating a campaign of state-sanctioned killings
to suppress an increasingly restive population.

The statement warns that such measures are losing their effectiveness, describing
Iran as “a powder keg” ready to ignite. It emphasizes the symbolic power
of those standing at the gallows, whose courage challenges the regime’s grip
on power.

Calling on both Iranian citizens and international organizations, the prisoners
urge a united effort to save the lives of Azizi, Ehsani, and Hassani and to end
the regime’s systematic use of the death penalty as a tool of repression.

The prisoners specifically appeal to the United Nations Human Rights Council,
the UN Special Rapporteur on the Situation of Human Rights in Iran, and other
human rights organizations to take immediate action.  Full text of the statement:

Once again, the decrepit and decaying regime, nearing the brink of collapse,
resorts to increased executions and massacres in an attempt to intimidate the
tormented society into submission and regression. The latest evidence of this
is the Supreme Court’s approval of the death sentences for three political
prisoners, Pakhshan Azizi, Behrouz Ehsani, and Mehdi Hassani. Meanwhile, more
than 50 other political and ideological prisoners remain on death row.

The criminal Supreme Leader, under whose approval, supervision, and essentially
direct orders all executions are carried out, believes that through a
wave of killings and executions, he can frighten the tormented people into
silence. However, this ultimate and primary tool of repression—execution—is
no longer a remedy for his incurable plight. Just as we have witnessed the
resilience of parents at the graves of their loved ones, we now see the spirit
of those standing at the gallows. Notably, these three political prisoners
sentenced to death were part of the “No to Executions Tuesdays” campaign
to save death-row prisoners. This signifies that even executions have lost
their effectiveness and may instead ignite the powder keg of an explosive Iran,
setting fire to the Supreme Leader’s cloak and the entire hollow regime.

Despite all this, we, the political prisoners in Qezel-Hesar Prison, while
condemning these death sentences, call upon our fellow free-spirited compatriots
to use all available resources and urge all international and human rights
organizations, particularly the UN Human Rights Council and the Special Rapporteur,
to take immediate action to save the lives of these three political prisoners.

The statement is signed by the following political prisoners in Qezel-Hesar Prison:

- Saeed Masouri
- Sepehr Emam Jomeh
- Mohammad Shafei
- Meysam Dehbanzadeh
- Loghman Aminpour

January 2025


68 prisonniers politiques demandent l’annulation de trois condamnations à mort en Iran
=============================================================================================

- https://wncri.org/fr/2025/01/11/68-prisonniers-politiques/

.. figure:: images/pakhshan_azizi_and_others_bis.webp

68 prisonniers politiques demandent l’annulation de trois condamnations à mort en Iran

Un groupe de prisonniers politiques, hommes et femmes, détenus dans les prisons
d’Evin, de Ghezel Hessar (Karadj), de Lakan (Racht) et du Grand Téhéran,
a publié une lettre ouverte demandant aux organisations et institutions
internationales d’utiliser toutes leurs capacités pour prendre des mesures
efficaces afin d’arrêter l’exécution de trois prisonniers politiques :
Pakhshan Azizi, Mehdi Hassani et Behrouz Ehsani.

Le 8 janvier 2025, la Cour suprême d’Iran a confirmé les condamnations à
mort de Pakhshan Azizi, Behrouz Ehsani Eslamlou et Mehdi Hassani (Lien anglais),
tous détenus à la prison d’Evin. Le tribunal révolutionnaire de Téhéran,
présidé par le juge Iman Afshari, les avait déjà condamnés à mort.

Le texte intégral de la lettre, signée par 68 prisonniers politiques en réponse à l’arrêt de la Cour suprême, est le suivant :
-------------------------------------------------------------------------------------------------------------------------------------

68 prisonniers politiques iraniens demandent l’abolition de la peine de mort

En poursuivant ses politiques intérieures et internationales destructrices, le
régime au pouvoir en Iran a plongé le pays dans un état de crise effroyable. La
solution du régime a consisté à intensifier la répression dans les domaines
social, politique et culturel.

Alarmé par la possibilité de nouvelles manifestations, le régime cherche à
maintenir son emprise sur le pouvoir en intensifiant le recours à la peine de
mort, créant ainsi une atmosphère de peur et de terreur. Rien qu’en 2024,
plus de 1 000 exécutions ont eu lieu en Iran, ce qui représente près de 75 %
de l’ensemble des exécutions dans le monde.

La Cour suprême a récemment confirmé les condamnations à mort de trois
prisonniers politiques – Pakhshan Azizi, Behrouz Ehsani et Mehdi Hassani-
faisant craindre leur exécution imminente.

Nous, un groupe de prisonniers politiques des prisons d’Evin, de Ghezel Hessar,
de Lakan et du Grand Téhéran, condamnons sans équivoque la peine de mort
comme une forme de meurtre commandité par l’Etat et appelons à l’abolition
complète de cette pratique inhumaine et réactionnaire.

En outre, nous exprimons notre soutien total à Pakhshan Azizi, Behrouz
Ehsani et Mehdi Hassani. Nous appelons le public iranien et les organisations
internationales, en particulier les organismes de défense des droits de l’Homme,
à mobiliser toutes leurs ressources pour sauver la vie de ces prisonniers
politiques dont l’exécution est imminente. Nous appelons également à des
efforts collectifs pour éradiquer la peine de mort dans son intégralité.

Signé par : 68 prisonniers politiques incarcérés à la prison d’Evin, à la prison de
Ghezel Hessar (Karadj), à la prison de Lakan (Racht) et à la prison du Grand
Téhéran.



Les 3 otages français en Iran : Cécile Kohler, Jacques Paris et Olivier Grondeau 📣
=================================================================================================

#FreeOlivier #FreeOlivierGrondeau #FreeThemAll #FreeJacquesParis #FreeCecileKohler

- :ref:`iran_luttes:otages_francais`

- :ref:`iran_luttes:cecile_kohler` |CecileKohler|
- :ref:`iran_luttes:jacques_paris` |JacquesParis|
- :ref:`iran_luttes:olivier_grondeau` |OlivierGrondeau| 


Cliquer sur https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3 
pour écouter l'appel à l'aide d'Olivier Grondeau depuis la prison d'Evin https://libertepourolivier.fr/

2024-12-09 Des prisons en Iran. **Laboratoire de cruauté et matrice de la violence d’État** contre une contestation sociale et politique sans précédent
===========================================================================================================================================================

- :ref:`iran_luttes_2024:prisons_iran_2024_12_09`

Préambule
==============

Suite à de nombreuses interventions dans les médias de Fariba Adelkhah, 
anthropologue et ancienne prisonnière en Iran, autrice du livre Prisonnière 
à Téhéran (Seuil, 2024), un collectif d’auteur·ice·s (liste complète en post-scriptum) 
a estimé utile de rappeler certains faits largement documentés, mais omis 
ou niés, concernant les prisons et plus largement la situation des droits 
humains en Iran. 

En vertu de la loi en la matière, nous publions à la suite de cet article 
(après la liste des signataires) un "droit de réponse" de Fariba Adelkhah 
qui nous a été adressé dimanche 22 décembre 2024, en laissant les lecteurs 
et lectrices juges de sa pertinence au regard du texte incriminé. 

Nous y joignons toutefois une brève mise au point des auteur·ice·es de la 
tribune, ainsi qu’un commentaire de notre collectif, mis en cause publiquement 
de manière injurieuse et mensongère par Jean-François Bayart, préfacier du 
livre de Fariba Adelkhah.

2025-01-14 51st Week of the “No to Execution Tuesdays” Campaign with Hunger Strikes in 34 Prisons Across Iran
===================================================================================================================

- https://iran-hrm.com/2025/01/14/51st-week-of-the-no-to-execution-tuesdays-campaign-with-hunger-strikes-in-34-prisons-across-iran/


# NoToExecutionTuesdaysCampaign #HungerStrike

Prisoners participating in the “No to Execution Tuesdays” Campaign across Iran 
marked the 51st week of their struggle by continuing their hunger strike 
on Tuesday, January 14, 2025. 

Recently, prisons in Haviq-Talesh, the women’s ward of Adelabad Prison in 
Shiraz, Borazjan, and Joveyn joined the Campaign.

This week’s campaign statement strongly condemned the confirmation of execution 
sentences for four political prisoners: Behrouz Ehsani, Mehdi Hassani, Pakhshan Azizi, 
and Mojahed Korkor.

.. figure:: images/campagne_51_2025_01_18.webp


La pétition
================

- https://www.change.org/p/nous-demandons-la-lib%C3%A9ration-de-l-activiste-humanitaire-iranienne-pakhshan-azizi

L'histoire de Pakhshan Azizi, une militante et travailleuse humanitaire iranienne,
nous touche profondément. Azizi, condamnée à mort par un tribunal iranien après
avoir été accusée de "rébellion", est actuellement détenue à la prison
d'Evin à Téhéran. Son arrestation a eu lieu en août 2023, lors d'un procès
que Amnesty International a dénoncé comme "grossièrement inéquitable". Azizi,
comme de nombreuses autres femmes militantes courageuses, est en isolement total,
soumise à un régime carcéral extrêmement sévère.

N'oublions pas que la prison d'Evin est la même où la journaliste Cecilia
Sala a été détenue, grâce à qui nous savons aujourd'hui que des centaines
de femmes, comme Azizi, sont emprisonnées simplement parce qu'elles ont osé
exprimer pacifiquement leur dissidence contre le régime. Les paroles de Sala nous
encouragent : "Ne les laissons pas seules, aidons-les ! Nous pouvons le faire !"

Il est temps de joindre nos forces et d'exiger que Pakhshan Azizi soit libérée
de prison, affirmant le droit universel à la liberté d'expression. Nous ne
pouvons pas permettre que la répression et la violation des droits humains se
poursuivent dans l'indifférence.

C'est pourquoi l'attention internationale peut faire la différence et
chaque signature peut contribuer à sauver la vie d'Azizi. Nous demandons que
l'activiste humanitaire iranienne Pakhshan Azizi soit pardonnée et libérée
de prison. Veuillez signer cette pétition !

Quelques photos
===================


.. figure:: images/20250118_143559_800.webp  
.. figure:: images/20250118_143747_800.webp  
.. figure:: images/20250118_143847_800.webp  
.. figure:: images/20250118_150140_800.webp  
.. figure:: images/20250118_154004_800.webp
.. figure:: images/20250118_143631_800.webp  
.. figure:: images/20250118_143801_800.webp  
.. figure:: images/20250118_143852_800.webp  
.. figure:: images/20250118_150201_800.webp



Articles
=============

- https://chk.me/ETFDpTq 
- :ref:`aiak:berivan_firat_2025_01_26`

