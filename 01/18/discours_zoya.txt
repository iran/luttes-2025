Le régime iranien tremble de tous parts, c'est le Mediapart qui nous parle,
avec des pénuries de gaz et d'électricité entraînant des manifestations dans
plusieurs villes, dans cet hiver particulièrement froid, avec des températures
allant jusqu'à moins 20, des usines à l'arrêt, les fermetures des écoles et
des administrations pendant plusieurs jours, les manifestations des retraités,
des ouvriers et de la désobéissance civile des femmes et toute la population,
la chute vertigineuse de la monnaie, la République islamique s'enfonce dans une
crise sans précédent. Après sa débâcle en Syrie, Téhéran s'inquiète du
retour de Donald Trump à la Maison Blanche. Le régime islamique est asphyxié et
en réponse à tout cela, il emprisonne et il tue, comme il a toujours fait depuis
son arrivée au pouvoir. Il a la deuxième place des plus grands exécuteurs du
monde après la Chine, depuis le mouvement Femmes Vies Libertés, le nombre des
exécutions a augmenté. L'exécution des femmes a augmenté de façon notable
pour accorder à l'Iran la première place d'exécuteur des femmes au monde. La
torture dans les prisons font des ravages. Amnesty International a recueilli 45
témoignages de viols de femmes, d'hommes et d'enfants. Plusieurs décennies ont
été faites. Des victimes, parfois mineures, se sont suicidées après leur
libération. Les tortures physiques et psychologiques demeurent des pratiques
courantes. Le refus des soins médicaux dont les prisonniers font l'objet
constitue aussi une forme de violence punitive. Dans des cellules d'isolement,
on oblige les prisonniers d'avouer ce qu'ils n'ont pas fait pour justifier
leur exécution. C'est en rejet de tout cela et en rejet de ce régime que des
femmes retirent leur voile obligatoire, symbole du pouvoir et de l'idéologie de
l'État. Les femmes revendiquent la liberté, la laïcité, la démocratie et la
justice sociale. Malgré une répression brutale et une précarité économique
galopante, elles chantent, dansent et proclamentent la justice sociale. Les femmes
sont en mesure de se réunir à la place de leurs parents. Les femmes sont en
mesure de se réunir à la place de leurs parents. Les hommes sont en mesure de
se réunir à la place de leurs parents. Les femmes sont en mesure de se réunir
à la place de leurs parents. La justice se défend et proclame l'urgence de la
liberté, car le pouvoir exige des corps tristes pour pouvoir les dominer. La juin
par conséquent est une résistance en Iran. Avec le mouvement Femmes Bilibertés,
la société iranienne rejette dans sa grande majorité l'islam politique et la
république islamique qui l'incarne. Cette semaine, il y a eu la condamnation
à mort de Parchan Azizi, un homme qui ne peut une activiste sociale kurde et
défenseur des droits de l'homme, ainsi que Mehdi Hassani et Behlouz Elham. La
condamnation de Farhan Azizi, qui a consacré sa vie entière à défendre
les droits des femmes, à lutter contre l'inégalité, a déclenché de vives
protestations. Le tribunal révolutionnaire l'a condamné pour des accusations
infondées de rébellion en raison des activités civiles et pacifiques, notamment
son aide aux femmes et aux enfants déplacés en Syrie et en Irak. Maï Sato,
le rapporteur spécial des Nations Unies sur les droits de l'homme pour l'Iran,
se dit profondément préoccupé par cette décision, en affirmant que ce cas
reflète un acte... d'un harcèlement plus large à l'encontre des femmes et des
militantes minoritaires. Le réseau de solidarité des femmes en Asie de l'Ouest
et en Afrique du Nord appelle la communauté internationale, les gouvernements,
les organisations de défense de l'homme et l'organisation de défense de femmes
à protester à une action mondiale pour empêcher le développement de droits
de l'homme. Pour empêcher cette exécution. En rajoutant que l'utilisation de
la peine de mort est une violation flagrante des droits humains fondamentaux. Il
appelle tous les défenseurs de la justice et des droits de l'homme pour condamner
cette injustice flagrante exigeant l'annulation de cette peine, démontrant
que le monde ne restera pas silencieux face à la cible des militants... des
droits des femmes. Le temps est compté. Unissons-nous pour prévenir cette
tragédie. L'organisation des médecins et des infirmiers du Kurdistan appelle
à une action immédiate pour sauver la vie de Parchan Azizi et celle de Barishe
Mouradi en appelant que... ... que... ... que... ... que... ... que... Parchan
Azizi n'a apporté que de l'aide humanitaire dans les camps en Syrie et n'a
en aucun cas eu des actions militaires. Cette organisation, tout en condamnant
cette décision, a demandé aux institutions civiles, juridiques et politiques de
résister à ce qu'elle appelle une vague d'injustice et a appelé la communauté
internationale à utiliser toutes les sources disponibles pour sauver la vie de
Parchan Azizi. L'association des enseignants de plusieurs villes en Kurdistan
également a condamné la condamnation à la mort de Parchan Azizi, a demandé à
la communauté internationale une action rapide et importante. Cette association
appelle aux responsables de la communauté internationale, aux responsables
juridictionnels, d'annuler toutes les condamnations à mort prononcées et allant
vers une élimination complète et permanente de la peine de mort en Iran dans
les textes et les lois pénales. Les syndicats des travailleurs de la compagnie de
bus en Iran également ont condamné la condamnation à la mort de Parchan Azizi
dans un rapport d'investi international et ont annoncé que parmi les activités
humanitaires de Parchan Azizi entre 2014 et 2022, sa participation à l'aide aux
femmes et aux enfants qui ont été déplacés après les attaques armées de
Daech a été notable. Le procès de Parchan Azizi, qui a été accompagné de
tortures et de confessions forcées, et de violations massives de droits de la
défense, est un exemple flagrant de l'expansion de la répression des activités
civiles et des défenseurs de droits de l'homme. Ce syndicat exige l'arrêt
immédiat de la peine de mort de Parchan Azizi, sa libération inconditionnelle,
ainsi que l'annulation de la peine de mort de Behlouz El Hamid et Mahdi Hassani,
Variché Mohammadi et d'autres activistes. Les mouvements ouvriers en Iran dans le
monde entier s'opposent fermement à la peine de mort sous toutes ses formes et
prétextes. Nous considérons l'abolition de la peine de mort comme une étape
humaine pour une justice durable. Nous appelons tous les travailleurs et les
organisations syndicales et les organisations de députés, les organisations
de défense des droits humains et les sociétés civiles internationales à
adopter une position sérieuse et efficace contre ces peines injustes et de la
répression systématique. Femmes, vie, liberté ! Femmes, vie, liberté ! Femmes,
vie, liberté ! Femmes, vie, liberté ! Femmes, vie, liberté ! Femmes, vie,
liberté ! Libérez les prisonniers politiques en Iran ! Libérez les prisonniers
politiques en Iran !
