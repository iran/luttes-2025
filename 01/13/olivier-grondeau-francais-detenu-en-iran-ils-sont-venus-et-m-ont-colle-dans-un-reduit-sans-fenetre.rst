.. index::
   pair: Olivier Grondeau ; Olivier Grondeau, Français détenu en Iran : "Ils sont venus et m’ont collé dans un réduit sans fenêtre (2025-01-13)

.. _olivier_grondeau_2025_01_13:

==========================================================================================================================================
2025-01-13 **Olivier Grondeau, Français détenu en Iran : "Ils sont venus et m’ont collé dans un réduit sans fenêtre"** Le Monde
==========================================================================================================================================

- https://www.lemonde.fr/idees/article/2025/01/13/olivier-grondeau-francais-detenu-en-iran-ils-sont-venus-et-m-ont-colle-dans-un-reduit-sans-fenetre_6496077_3232.html
- :ref:`iran_luttes:olivier_grondeau`
- :ref:`le_monde_2025_01_15`


#FreeOlivier #FreeOlivierGrondeau #FreeThemAll #FreeJacquesParis #FreeCecileKohler

Appel à l'aide d'Olivier Grondeau depuis la prison d'Evin
===========================================================

- https://libertepourolivier.fr/appel-a-l-aide
- https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3

Cliquer sur https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3 
pour écouter l'appel à l'aide d'Olivier Grondeau depuis la prison d'Evin.

Si seulement l’innocence rendait immortel |OlivierGrondeau|
==================================================================

Arrêté en octobre 2022 alors qu’il voyageait dans la République islamique et 
détenu à la prison d’Evin, le Français Olivier Grondeau, 34 ans, a fait 
parvenir au "Monde" une lettre interpellant les instances internationales 
afin d’accélérer sa libération et celle des autres otages retenus en Iran

Olivier Grondeau, voyageur originaire de Paris, est retenu en otage en Iran 
depuis plus de deux ans. Il a été arrêté arbitrairement lors d’un séjour dans la
ville de Chiraz (dans le sud du pays), le 12 octobre 2022, puis incarcéré dans
les prisons de Chiraz et d’Evin (à Téhéran), et avait jusqu’à présent
souhaité éviter la médiatisation de sa situation. 

Cet ancien champion de Scrabble junior, grand lecteur et écrivain, a fait 
une grève de la faim en janvier 2024 en l’absence de perspectives quant à la 
date de son procès. 
Le 19 février 2024, il a été condamné à cinq ans de prison ferme pour 
"collecte d’informations en vue de remettre au service d’espionnage adverse", 
des charges qu’il réfute catégoriquement. 
La peine a été confirmée en appel, en mai 2024.  

Ses conditions de détention sont particulièrement précaires : Olivier
a droit à des appels hebdomadaires à ses parents et à des appels sur des
numéros iraniens en fonction des créneaux alloués arbitrairement. 
A bout de forces, il a décidé de sortir de l’anonymat, à l’instar des deux autres
Français retenus par le régime des mollahs, Cécile Kohler et Jacques Paris,
au nom desquels il s’exprime aussi.


Il y a des mots qu’il nous coûte d’employer. Qui sont trop coupants pour ne
pas nous distraire de cette tâche à laquelle on s’est attelé : survivre. 
Alors celles et ceux qui portent notre parole nous aident, ces mots-là, ils nous
en soulagent. D’autres, en revanche, ne sauraient être dits que par nous.
L’innocence. Cette chose éblouissante, cette chose aveuglante qu’est
l’innocence. Il ne s’agit pas ici de l’établir : mon innocence est tout
établie. Je ne suis rien d’autre qu’une monnaie d’échange. Nos ravisseurs
s’en cachent à peine et les juges l’avouent à huis clos. 
Mais est-ce que ça suffit ? Qu’a-t-on dit de l’innocence une fois qu’on l’a 
prouvée ?

J’ai rencontré l’innocence le 12 octobre 2022. Quatre hommes m’attendaient
à la porte de ma chambre d’hôtel. C’étaient mes ravisseurs. Et là,
tapie dans leur ombre, c’était mon innocence. J’étais trop bouleversé
pour la remarquer alors. Il s’est passé un long moment avant que je commence
à sentir sa présence.

"Je l’ai trouvée devant ma porte, un soir que je rentrais chez moi…"
Barbara chante ainsi la solitude, et la ressemblance m’a frappé. C’est vrai,
l’innocence, "avec ses larges yeux cernés", a partie liée avec un certain
type de solitude. L’enlèvement, d’abord, nous esseule : ils sont venus,
m’ont découpé de la scène heureuse d’un matin d’automne et m’ont
collé dans un réduit sans fenêtre. Ils m’ont esseulé à moi-même par un
bandeau sur les yeux. Et puis l’interrogatoire, qui affirmait qu’au milieu
de la foule insouciante ma silhouette avait été découpée car j’étais
coupable. Or, tandis qu’après m’avoir dépossédé du monde et de moi-même,
ils me dépossédaient de la vérité, les voilà qui m’attribuaient une qualité
nouvelle. Ce qui s’est élevé soudain de l’ombre accumulée de leurs papiers,
de leurs dossiers et de leurs traductions, c’est l’édifice de mon innocence.

"Pourquoi me croient-ils coupable ?"
===========================================

Les semaines ont passé. Je niais avec force – ce qui suffit, du point de vue 
du droit. 
Je savais bien que je n’étais pas coupable. Pourtant, inexplicablement, une 
ligne imaginaire, un genre de tropique, m’empêchait de penser mon innocence. 
Ce n’est pas que je rechignais à employer le mot, c’est qu’il manquait soudain à
mon vocabulaire. 

Ça a duré des mois. Je me dis, maintenant, que l’idole de l’innocence, c’est 
moi qui l’avais craintivement cachée derrière un voile, et qu’en la libérant, 
j’aurais dû affronter tous ces autres esprits qui lui étaient asservis : l’erreur 
judiciaire, le procès au long cours, voire la justice arbitraire et la prise 
d’otage politique. 

Et ces mots-là, les premiers temps, quand l’espoir de retrouver la vie d’avant 
n’a pas encore achevé de nous couler entre les doigts, ces mots-là, on n’en 
veut pas. On ne veut pas "en être arrivé là".  
Alors on se rencogne et on attend. Pendant que les lampes nous imposent un 
jour ininterrompu, la peur, elle, nous maintient dans une nuit artificielle. 

Au cœur de leur nuit, je ne m’employais plus qu’à cela : défaire la tapisserie 
de leurs accusations. 
Une question surtout m’épuisait : pourquoi me croyaient-ils coupables ? 
Jusqu’à aujourd’hui, pas un jour n’a passé sans que je commette cette éblouissante 
méprise : penser qu’ils se sont trompés. 

Après quinze mois d’une instruction à vide, après les aveux des juges que 
le verdict ne dépendait pas d’eux, revenait jour après jour cette instinctive 
et désarmante confiance en l’intégrité de l’autre : "Pourquoi m’ont-ils cru coupable ?" 

Mais bien sûr qu’ils n’y ont jamais cru ! Que nous sommes les victimes de 
stratégies de pouvoir dont les enjeux et l’immoralité nous sont absolument, 
intrinsèquement étrangers ! 
Bien sûr que dès ma première nuit de détention, ils me savaient innocent ! 
Que ce n’est pas une privation de liberté qu’ils ordonnent au soir du 12 octobre 2022,
mais une séquestration !

Pourtant, rien n’a eu raison de cette question. "Partout, elle me fait
escorte, elle est revenue la voilà…" Au plus fort du désastre : la
grâce ? Cette grâce de l’innocente ou de l’innocent qui, à la suite de
l’attaque qui a rasé sa rationalité, cherche, malgré elle, malgré lui,
sous les décombres et dans les débris d’obus, un signe de l’humanité de
ses agresseurs. Jamais, j’en suis persuadé, un seul de mes ravisseurs n’a pu
imaginer que, de retour dans ma cellule (et encore aujourd’hui !), la question
qui m’obsédait fût celle-ci : "Pourquoi me croient-ils coupable ?" Jamais
ils n’ont pu concevoir que ce qui me préoccupait à ce moment-là, c’était
de les disculper. Liraient-ils une traduction de cette tribune qu’ils n’en
croiraient pas un mot. Heureusement : ils auraient tant à assumer soudain. Et
après une telle éclipse, mon innocence ne les brûlerait-elle pas ? Je ne peux
quand même pas leur souhaiter ça.

Voilà ce qui m’a fait acquérir la certitude que, dans ces lugubres
moments passés en cellule à défaire leur tapisserie, je tissais déjà la
mienne. J’agissais bien davantage que ce que ma vision floutée de stupeur
et dévorée par le bandeau ne le laissait supposer. Eclairé quand il faut
dormir et aveuglé quand il faut marcher, éprouvé par un arsenal de tortures
simples comme le pain, trop frappé d’une perpétuelle stupeur pour rendre
consciente l’innocence, voyons pourtant quelle alternative radicale et
bouleversante incarne l’innocent… Depuis le jour où nos ravisseurs sont
venus nous chercher, il n’y a jamais eu que ça : façonner leur temps à
notre image. Heure après heure, je tissais. Dans le plus grand secret, la
nuque docilement offerte aux lucarnes et aux caméras, à l’insu de tous et de
moi-même, je tissais. Peut-être me l’étais-je passé moi-même, ce second
bandeau qui couvrait ma conscience, pour tisser en paix – car le fil était si
fragile.  Alors on tisse, de fragiles motifs, instinctivement. C’est-à-dire :
on vit. Une vie curieuse, comme juchée au-dessus de nous-mêmes. Une survie.

Et les mois ont passé. C’est déconcertant comme on a réussi à vivre. Vous
comprenez qu’après vingt-sept mois entre leurs mains, je trouve ça
déconcertant. Vous comprenez qu’après deux ans et huit mois dans une
cellule éclairée jour et nuit, au cœur de la contrainte, de la torture,
de l’arbitraire et de la peur, il est déconcertant que Cécile et Jacques
vivent encore ?

Une chose éblouissante 
=========================

En un sens, nous n’avons rien à voir avec notre
innocence. C’est, de la part de nos ravisseurs, une sorte d’apposition des
mains. Extérieure à nous, elle nous survivra, poursuivant sa croissance chaque
jour qui passe et qui la nie.  Jeudi encore a été diffusé un reportage montrant
que chez nous, dans les prisons d’Iran, on a beau mourir parfois, la plupart du
temps on danse. Ni le mensonge, ni le cynisme, ni le meurtre n’auront raison de
notre innocence. La seule manière de vous protéger de son insoutenable éclat,
ce n’est pas de nous laisser mourir, c’est de nous libérer.  Car il ne
faut pas s’y méprendre : l’éclat de notre innocence n’est ni un indice
de notre résilience, ni une manifestation de notre vitalité. Qu’est-ce que
cela dit de nous ? Pas qu’on s’en sortira. Pas qu’on verra l’été. Si
seulement l’innocence rendait immortel ! Oh, il en faut, des flèches, et
profond est l’émoi qui traverse alors les gradins, mais ils sont nombreux,
ceux qui finissent par s’effondrer dans la poussière.

Non, ça dit de l’innocence qu’elle est une chose, oui, éblouissante.
=========================================================================

Mais inquiétante aussi, quand elle se met à briller d’un pareil éclat. 
Cet éclat n’est pas le reflet de nos forces vitales. Peut-être permet-il au
contraire d’en estimer la déperdition. A mesure que croît sa lumière, la
nôtre s’éteint. 
Trop épuisés pour ménager davantage la susceptibilité de nos ravisseurs, 
Cécile, Jacques et moi atteignons cette période éphémère où notre innocence 
se manifeste dans son plus vif éclat, flamboie comme elle n’a jamais flamboyé. 
Rugit comme l’incendie. Et soudain nous nous éteindrons, consumés.

Olivier Grondeau est détenu dans la prison d’Evin, à Téhéran. 

Il a été condamné à cinq ans de prison ferme, sa peine a été confirmée en 
appel en mai 2024
