

.. _syndicats_2025_01_10:

================================================================================================
2025-01-10 **Iran : Halte à la répression contre les syndicalistes de l'enseignement**
================================================================================================


:download:`Tract au format PDF <pdfs/iran_halte_a_la_repression_2025_01_10.pdf>`


Iran : Halte à la répression contre les syndicalistes de l'enseignement
===========================================================================

Le 12 octobre 2024, l’avocat de Mohammad Habibi, porte-parole de l’Association
des enseignantes et enseignants d'Iran, a annoncé que la cour d’appel
de Téhéran avait confirmé la décision du tribunal révolutionnaire de
Shahryar. 

Ainsi, le 5 novembre 2024, Mohammad Habibi a été convoqué au pôle
d’application des peines de Shahryar. 
Le 11 novembre, après s’y être présenté avec son avocat, il a été arrêté et 
transféré à la prison d’Evin à Téhéran.  

Cette condamnation de six mois et un jour pour propagande contre le régime 
est en réalité liée à ses protestations contre les empoisonnements dans les 
établissements scolaires, surtout ceux pour filles. 

Elle fait partie de mesures visant à réduire au silence les militants pour 
la justice et la transparence politique.  
Les empoisonnements ont débuté fin novembre 2022 et se sont intensifiés au 
printemps 2023, suscitant inquiétudes et théories variées, allant de misogynie 
systémique à psychose collective. 
Le gouvernement a ignoré l'utilisation de substances toxiques contre 
l'éducation des filles et refusé les enquêtes indépendantes. 
Des arrestations et aveux confus ont suivi, attribués par le régime aux 
ennemis de l'Iran ou à l'hystérie collective. 

Cependant, ces événements soulèvent des questions sur les liens possibles avec 
le régime et l'extrémisme religieux, rendant le gouvernement responsable aux 
yeux des militants et du public.  

Mohammad Habibi et ses collègues estiment que les syndicalistes doivent 
soutenir les revendications des enseignants et défendre les droits des élèves. 

Ils pensent que les protestations des enseignants reflètent une souffrance 
générale et peuvent mener à plus de manifestations. 
C'est pour cela que des dizaines d'enseignants sont emprisonnés et des 
centaines sont licenciés pour leur soutien au mouvement Femme, Vie, Liberté. 

Le régime souhaite ainsi prévenir d'autres mouvements collectifs en emprisonnant 
les militants syndicalistes.  
Mohsén Omrani, membre du conseil d’administration de l’Association des 
enseignants de Bouchehr, a commencé à purger une peine de six mois de prison 
le 11 novembre 2024.
Enseignant devenu vendeur ambulant par nécessité économique, il a subi
diverses répressions après le soulèvement de Jina, comme des licenciements,
emprisonnements et amendes.  

Défenseur des droits des enfants et des femmes, Mohsén Omrani parle de 
l'enseignement en langue maternelle comme un droit fondamental. 
Avant son incarcération, il a plaidé pour des adhésions massives aux associations 
professionnelles, croyant que l'union est essentielle pour atteindre leurs 
objectifs.  

Nos organisations syndicales dénoncent le harcèlement des syndicalistes en 
Iran font campagne pour que le gouvernement iranien respecte les libertés 
fondamentales dont les droits syndicaux.  

Face aux dérives de ce régime, la solidarité internationale est plus que 
jamais indispensable.

Libération immédiate des syndicalistes et prisonnie.res d'opinion en Iran !


Paris, le 10/01/2025
