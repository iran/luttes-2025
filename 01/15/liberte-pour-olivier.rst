.. index::
   pair: newsletter ; Liberté pour Olivier (2025-01-15)

.. _newsletter_olivier_grondeau_2025_01_15:

==========================================================================================================================================
2025-01-15 **Liberté pour Olivier**  par Collectif Liberté pour Olivier
==========================================================================================================================================

- :ref:`iran_luttes:olivier_grondeau`
- https://libertepourolivier.fr
- https://libertepourcecile.com/
- https://libertepourjacques.com/

#FreeOlivier #FreeOlivierGrondeau #FreeThemAll #FreeJacquesParis #FreeCecileKohler

Appel à l'aide d'Olivier Grondeau depuis la prison d'Evin
===========================================================

- https://libertepourolivier.fr/appel-a-l-aide
- https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3

Cliquer sur https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3 
pour écouter l'appel d'Olivier Grondeau depuis la prison d'Evin.

Liberté pour Olivier |OlivierGrondeau|
===========================================

Le collectif Liberté pour Olivier, ses ami·es, ses parents, sa famille, ses
proches, tous et toutes tenons à vous remercier pour votre soutien.

Cette infolettre a pour objectif de relayer les actualités autour de
nos actions et de la situation d’Olivier. 

Une première vague médiatique réussie
===========================================

France Inter
----------------

- https://www.radiofrance.fr/franceinter/podcasts/l-invite-de-7h50/l-invite-de-7h50-du-lundi-13-janvier-2025-6316276

Nous avons lancé la médiatisation de la situation d’Olivier lundi 13 janvier 2025
au petit matin, avec l’intervention de sa maman Thérèse et de son ami Tristan
à la matinale de France Inter, avec un extrait audio enregistré par Olivier
et diffusé toute la journée.  

Retrouvez l’émission `ici https://www.radiofrance.fr/franceinter/podcasts/l-invite-de-7h50/l-invite-de-7h50-du-lundi-13-janvier-2025-6316276 <https://www.radiofrance.fr/franceinter/podcasts/l-invite-de-7h50/l-invite-de-7h50-du-lundi-13-janvier-2025-6316276>`_


.. _le_monde_2025_01_15:

Le Monde (Olivier Grondeau, Français détenu en Iran : "Ils sont venus et m’ont collé dans un réduit sans fenêtre)
-------------------------------------------------------------------------------------------------------------------------

- :ref:`olivier_grondeau_2025_01_13`

Pour porter sa voix au maximum, Le Monde a publié dans son édition papier du
15 janvier une tribune très émouvante écrite par Olivier lui-même depuis sa
cellule de prison.  

Vous pouvez la lire :ref:`olivier_grondeau_2025_01_13`

Brut
---------

- https://www.youtube.com/shorts/WWSa4SlbMW0

Hier, Brut diffusait un entretien avec le ministre des affaires étrangères,
où celui-ci rendait hommage au courage d’Olivier et était pressé par le
journaliste de répondre à des questions concernant la situation des otages en
Iran. C’est un premier pas !  

Regarder `l’extrait <https://www.youtube.com/shorts/WWSa4SlbMW0>`_


Liberté pour Olivier
-----------------------------

- https://libertepourolivier.fr/appel-a-l-aide
- https://libertepourolivier.fr/premiere-journee-de-mediatisation

Vous pourrez retrouver sur notre site `l’appel à l’aide d’Olivier <https://libertepourolivier.fr/appel-a-l-aide>`_ 
dans son intégralité ainsi qu’une `sélection des interventions de Thérèse et Tristan <https://libertepourolivier.fr/premiere-journee-de-mediatisation>`_
et des articles parus dans la journée du 13 janvier 2025.  

Maintenons la pression
============================

C’est un grand soulagement de voir que les médias français ont été
réactifs. 

Nous espérons que cette visibilité aura pour effet de rappeler aux instances 
au pouvoir qu’il est impératif qu’Olivier, Cécile et Jacques soient libérés 
à très courte échéance. 

Grâce à vous qui en parlez, qui partagez, qui likez, le retentissement médiatique 
ne faiblit pas. Continuez !

Une lettre pré-rédigée pour interpeller vos élu·es et les enjoindre à se saisir du sujet
-----------------------------------------------------------------------------------------------

- https://libertepourolivier.fr/aider#:~:text=interpeller%20votre%20depute%C2%B7e (Interpeller votre député·e)
- https://www.assemblee-nationale.fr/dyn/vos-deputes


Nous profitons de cet espace pour vous informer que nous venons de mettre
à disposition une lettre pré-rédigée pour interpeller vos élu·es et
les enjoindre à se saisir du sujet. 
Nous l’avons déjà envoyée au nom du collectif et avons besoin que vous le 
fassiez à titre personnel pour insister sur l’urgence de la situation. 
Si vous le pouvez et le souhaitez bien sûr.

Accéder à la page `Aider <https://libertepourolivier.fr/aider#:~:text=interpeller%20votre%20depute%C2%B7e>`_

Nous vous remercions encore pour votre soutien
-----------------------------------------------------

Nous vous remercions encore pour votre soutien, quelle que soit sa forme, et
vous assurons de notre totale mobilisation pour qu’on ne cesse pas d’entendre
parler d’Olivier.

À bientôt, Le Collectif Liberté pour Olivier https://libertepourolivier.fr
